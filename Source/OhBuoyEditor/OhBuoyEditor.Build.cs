using UnrealBuildTool;

public class OhBuoyEditor : ModuleRules
{
    public OhBuoyEditor(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "OhBuoy" });

        PrivateDependencyModuleNames.AddRange(new string[] { "UnrealEd" });
    }
}