#include "SailComponentVisualizer.h"
#include "OhBuoy/Boat/SailComponent.h"
#include "Kismet/KismetSystemLibrary.h"

void FSailComponentVisualizer::DrawVisualization(const UActorComponent* Component, const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	const USailComponent* sailComponent = Cast<const USailComponent>(Component);

	if (sailComponent == nullptr)
	{
		return;
	}

	FTransform componentTransform = sailComponent->GetComponentTransform();
	for (const FSail& sail : sailComponent->GetSails())
	{
		PDI->DrawLine(sail.Point1, sail.Point2, FLinearColor::Blue, 1.f, 3.f);
		PDI->DrawLine(sail.Point2, sail.Point3, FLinearColor::Blue, 1.f, 3.f);
		PDI->DrawLine(sail.Point3, sail.Point1, FLinearColor::Blue, 1.f, 3.f);
	}
}
