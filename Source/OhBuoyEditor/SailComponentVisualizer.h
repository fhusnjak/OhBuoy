#pragma once

#include "ComponentVisualizer.h"

class OHBUOYEDITOR_API FSailComponentVisualizer : public FComponentVisualizer
{
public:
	virtual void DrawVisualization(const UActorComponent* Component, const FSceneView* View, FPrimitiveDrawInterface* PDI) override;
};
