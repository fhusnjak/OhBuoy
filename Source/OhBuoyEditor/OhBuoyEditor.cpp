#include "OhBuoyEditor.h"
#include "UnrealEd.h"
#include "OhBuoy/Boat/SailComponent.h"
#include "SailComponentVisualizer.h"

IMPLEMENT_GAME_MODULE(FOhBuoyEditorModule, OhBuoyEditor);

void FOhBuoyEditorModule::StartupModule()
{
	if (GUnrealEd)
	{
		TSharedPtr<FComponentVisualizer> visualizer = MakeShareable(new FSailComponentVisualizer());
		GUnrealEd->RegisterComponentVisualizer(USailComponent::StaticClass()->GetFName(), visualizer);
		visualizer->OnRegister();
	}
}

void FOhBuoyEditorModule::ShutdownModule()
{
	if (GUnrealEd)
	{
		GUnrealEd->UnregisterComponentVisualizer(USailComponent::StaticClass()->GetFName());
	}
}
