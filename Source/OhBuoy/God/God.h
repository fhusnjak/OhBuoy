#pragma once

#include "God.generated.h"

UCLASS(Blueprintable)
class OHBUOY_API AGod : public APawn
{
	GENERATED_BODY()

public:
	AGod(const FObjectInitializer& initializer);

	virtual void BeginPlay() override;
};
