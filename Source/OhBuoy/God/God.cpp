#include "God.h"

AGod::AGod(const FObjectInitializer& initializer)
	: Super(initializer)
{
}

void AGod::BeginPlay()
{
	SetActorLocation(FVector(20000.f, -10000.f, 17000.f));
	SetActorRotation(FRotator::ZeroRotator.Quaternion());
}
