#include "AdvancedBuoyancyComponent.h"
#include "AdvancedBoat.h"
#include "DrawDebugHelpers.h"
#include "Math/NumericLimits.h"
#include "Net/UnrealNetwork.h"
#include "PhysXPublic.h"
#include "PhysicsEngine/BodySetup.h"

UAdvancedBuoyancyComponent::UAdvancedBuoyancyComponent(const FObjectInitializer& initializer)
	: Super(initializer)
{
	PrimaryComponentTick.TickGroup = TG_PrePhysics;
	PrimaryComponentTick.bCanEverTick = true;
	bAutoActivate = true;
	UActorComponent::SetComponentTickEnabled(true);
}

void UAdvancedBuoyancyComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

bool UAdvancedBuoyancyComponent::IsUnderWater() const
{
	return _isUnderWater;
}

void UAdvancedBuoyancyComponent::TickComponent(float deltaTime, enum ELevelTick tickType, FActorComponentTickFunction *thisTickFunction)
{
	Super::TickComponent(deltaTime, tickType, thisTickFunction);

	AAdvancedBoat* boat = GetOwner<AAdvancedBoat>();
	if (boat == nullptr || boat->GetMeshComponent() == nullptr)
	{
		return;
	}

	_isUnderWater = GetHeightAboveWater(boat->GetMeshComponent()->GetComponentTransform().TransformPosition(FVector(-240, 0, -50))) < 0.f;

	if (!_initialized)
	{
		Initialize();
		_initialized = true;
	}

	ApplyMeshForces(deltaTime);
}

void UAdvancedBuoyancyComponent::DrawDebugTriangle(const FVector& a, const FVector& b, const FVector& c, const FColor& color, const float thickness)
{
	DrawDebugLine(GetWorld(), a, b, color, false, -1.f, 0, thickness);
	DrawDebugLine(GetWorld(), b, c, color, false, -1.f, 0, thickness);
	DrawDebugLine(GetWorld(), c, a, color, false, -1.f, 0, thickness);
}

void UAdvancedBuoyancyComponent::Initialize()
{
	_vertices.Empty();
	_triangleData.Empty();
	_buoyantMeshArea = 0.f;
	if (const UBodySetup* bodySetup = GetBodySetup())
	{
		for (const physx::PxTriangleMesh* triangleMesh : bodySetup->TriMeshes)
		{
			const PxVec3* const verticesPx = triangleMesh->getVertices();
			const PxU32 vertexCount = triangleMesh->getNbVertices();

			for (PxU32 i = 0; i < vertexCount; ++i)
			{
				_vertices.Emplace(P2UVector(verticesPx[i]));
			}

			// Get triangles
			const PxU32 triangleCount = triangleMesh->getNbTriangles();
			const void* const indicesPx = triangleMesh->getTriangles();
			const auto b16BitIndices = triangleMesh->getTriangleMeshFlags() & PxTriangleMeshFlag::e16_BIT_INDICES;

			TArray<int32> indices;
			for (PxU32 i = 0; i < triangleCount * 3; ++i)
			{
				if (b16BitIndices)
				{
					indices.Add(static_cast<const PxU16*>(indicesPx)[i]);
				}
				else
				{
					indices.Add(static_cast<const PxU32*>(indicesPx)[i]);
				}
			}

			for (uint32 i = 0; i < triangleCount; i++)
			{
				const int32 index1 = indices[i * 3 + 0];
				const int32 index2 = indices[i * 3 + 1];
				const int32 index3 = indices[i * 3 + 2];
				const FVector& a = _vertices[index1].LocalPosition;
				const FVector& b = _vertices[index2].LocalPosition;
				const FVector& c = _vertices[index3].LocalPosition;

				float area = GetTriangleArea(a, b, c);
				_triangleData.Emplace(index1, index2, index3, area);

				_buoyantMeshArea += area;
			}
		}
	}
}

void UAdvancedBuoyancyComponent::UpdateTriangleData(const FBuoyantMeshSubmergedTriangle& submergedTriangle)
{
	AAdvancedBoat* boat = Cast<AAdvancedBoat>(GetOwner());
	if (boat == nullptr || boat->GetMeshComponent() == nullptr)
	{
		return;
	}
	
	FBuoyantMeshTriangleData& data = _triangleData[submergedTriangle.OriginalTriangleIndex];

	data.Normal = submergedTriangle.Normal;

	data.PreviousSubmergedArea = data.SubmergedArea;
	data.SubmergedArea = submergedTriangle.GetArea();

	FVector center = submergedTriangle.GetCenter();
	data.Center = {center, GetHeightAboveWater(center)};

	data.PreviousVelocity = data.Velocity;
	data.Velocity = boat->GetWaterRelativeVelocityAtPoint(center);
}

FTriangleForce UAdvancedBuoyancyComponent::GetSubmergedTriangleForce(const FBuoyantMeshTriangleData& triangleData, float cf, float deltaTime) const
{
	AAdvancedBoat* boat = Cast<AAdvancedBoat>(GetOwner());
	if (boat == nullptr)
	{
		return FTriangleForce{ FVector::ZeroVector, FVector::ZeroVector };
	}

	if (FMath::IsNearlyZero(triangleData.Area))
	{
		return FTriangleForce{ FVector::ZeroVector, FVector::ZeroVector };
	}

	FVector force = FVector::ZeroVector;

	const FVector staticForce = FBuoyantMeshSubmergedTriangle::GetHydrostaticForce(_waterDensity, _gravityMagnitude, triangleData);
	force += staticForce;

	//TRACE_LOG("Area = %f, SubmergedArea = %f, Height = %f, ", triangleData.Area, triangleData.SubmergedArea, triangleData.Center.Height);

	const FVector dynamicForce = FBuoyantMeshSubmergedTriangle::GetHydrodynamicForce(GetWorld(), _waterDensity, cf, _cPD1, _cPD2, _fP, _cSD1, _cSD2, _fS, _p, _maxAcceleration, _buoyantMeshArea, boat->GetMass(), deltaTime, triangleData);
	force += dynamicForce;

	//DrawDebugLine(GetWorld(), triangleData.Center.Position, triangleData.Center.Position - dynamicForce.GetSafeNormal() * 50.f, FColor::Red, false, -1.f, 0, 1.f);

	return FTriangleForce{ force, triangleData.Center.Position };
}

void UAdvancedBuoyancyComponent::ApplyMeshForces(float deltaTime)
{
	AAdvancedBoat* boat = Cast<AAdvancedBoat>(GetOwner());
	if (boat == nullptr || boat->GetMeshComponent() == nullptr)
	{
		return;
	}

	static TArray<FBuoyantMeshSubmergedTriangle> submergedTriangles;

	const FTransform localToWorld = GetComponentTransform();

	float maxX = FLT_MIN;
	float minX = FLT_MAX;
	int32 currentSubmergedTriangle = 0;
	int32 triangleIndex = 0;

	for (FBuoyantMeshVertex& vertex : _vertices)
	{
		vertex.Position = localToWorld.TransformPosition(vertex.LocalPosition);
		vertex.Height = GetHeightAboveWater(vertex.Position);
	}
	
	submergedTriangles.Empty(submergedTriangles.Num());
	for (FBuoyantMeshTriangleData& triangle : _triangleData)
	{
		const FBuoyantMeshVertex& a = _vertices[triangle.A];

		const FBuoyantMeshVertex& b = _vertices[triangle.B];

		const FBuoyantMeshVertex& c = _vertices[triangle.C];

		const FBuoyantMeshTriangle buoyantMeshTriangle = FBuoyantMeshTriangle::FromClockwiseVertices(a, b, c);
		buoyantMeshTriangle.GetSubmergedTriangles(submergedTriangles);

		for (int32 i = currentSubmergedTriangle; i < submergedTriangles.Num(); i++)
		{
			FBuoyantMeshSubmergedTriangle& submergedTriangle = submergedTriangles[i];
			submergedTriangle.OriginalTriangleIndex = triangleIndex;

			maxX = FMath::Max(maxX, submergedTriangle.A.X);
			maxX = FMath::Max(maxX, submergedTriangle.B.X);
			maxX = FMath::Max(maxX, submergedTriangle.C.X);

			minX = FMath::Min(minX, submergedTriangle.A.X);
			minX = FMath::Min(minX, submergedTriangle.B.X);
			minX = FMath::Min(minX, submergedTriangle.C.X);
		}

		triangleIndex++;
		currentSubmergedTriangle = submergedTriangles.Num();
	}

	float length = maxX - minX;
	float velocity = boat->GetWaterRelativeVelocityAtPoint(GetComponentTransform().TransformPosition(FVector::ZeroVector)).Size();
	float rn = (velocity * length) * 0.0001f / _waterKinematicViscosity;
	float cf = 0.075f / FMath::Pow(FMath::Log2(rn) / FMath::Log2(10.f) - 2.f, 2.f);

	for (const FBuoyantMeshSubmergedTriangle& submergedTriangle : submergedTriangles)
	{
		DrawDebugTriangle(submergedTriangle.A, submergedTriangle.B, submergedTriangle.C, FColor::Green, 1.f);

		UpdateTriangleData(submergedTriangle);
		ApplyMeshForce(GetSubmergedTriangleForce(_triangleData[submergedTriangle.OriginalTriangleIndex], cf, deltaTime));
	}
}

void UAdvancedBuoyancyComponent::ApplyMeshForce(const FTriangleForce& force)
{
	AAdvancedBoat* boat = Cast<AAdvancedBoat>(GetOwner());
	if (boat == nullptr || boat->GetMeshComponent() == nullptr)
	{
		return;
	}

	if (force.Vector.IsNearlyZero() || force.Vector.ContainsNaN())
	{
		return;
	}

	// Convert Newton to UE measuring unit (kg m s^-2 to kg cm s^-2)
	FVector ueForce = 100.f * force.Vector;
	boat->GetMeshComponent()->AddForceAtLocation(ueForce, force.Point);
}

float UAdvancedBuoyancyComponent::GetHeightAboveWater(const FVector& position) const
{
	AAdvancedBoat* boat = Cast<AAdvancedBoat>(GetOwner());
	if (boat == nullptr)
	{
		return 0.f;
	}

	float heightAboveWater = FLT_MAX;

	for (AWaterBody* waterBody : boat->GetCurrentWaterBodies())
	{
		if (waterBody == nullptr)
		{
			continue;
		}

		EWaterBodyQueryFlags queryFlags = EWaterBodyQueryFlags::ComputeLocation | EWaterBodyQueryFlags::IncludeWaves;
		FWaterBodyQueryResult queryResult = waterBody->QueryWaterInfoClosestToWorldLocation(position, queryFlags);
		float newHeightAboveWater = position.Z - queryResult.GetWaterSurfaceLocation().Z;
		heightAboveWater = FMath::Min(heightAboveWater, newHeightAboveWater);
	}

	return heightAboveWater;
}
