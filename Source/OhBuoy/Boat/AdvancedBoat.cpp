#include "AdvancedBoat.h"
#include "AdvancedBuoyancyComponent.h"
#include "SailComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "MotionControllerComponent.h"

AAdvancedBoat::AAdvancedBoat(const FObjectInitializer& initializer)
	: Super(initializer)
{
	_skeletalMeshComponent = initializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("SkeletalMeshComponent"));
	_skeletalMeshComponent->SetSimulatePhysics(true);
	_skeletalMeshComponent->SetEnableGravity(true);
	SetRootComponent(_skeletalMeshComponent);

	_advancedBuoyancyComponent = initializer.CreateDefaultSubobject<UAdvancedBuoyancyComponent>(this, TEXT("BuoyancyComponent"));
	_advancedBuoyancyComponent->SetupAttachment(_skeletalMeshComponent);

	_sailComponent = initializer.CreateDefaultSubobject<USailComponent>(this, TEXT("SailComponent"));
	_sailComponent->SetupAttachment(_skeletalMeshComponent);

	_mainSailConstraint = initializer.CreateDefaultSubobject<UPhysicsConstraintComponent>(this, TEXT("MainSailPhysicsConstraint"));
	_mainSailConstraint->SetupAttachment(_skeletalMeshComponent);

	_springArmComponent = initializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("SpringArmComponent"));
	_springArmComponent->SetupAttachment(_skeletalMeshComponent);
	_cameraComponent = initializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("CameraComponent"));
	_cameraComponent->SetupAttachment(_springArmComponent);

	_leftHand = initializer.CreateDefaultSubobject<UMotionControllerComponent>(this, TEXT("LeftMotionController"));
	_leftHand->SetupAttachment(_cameraComponent);
	_rightHand = initializer.CreateDefaultSubobject<UMotionControllerComponent>(this, TEXT("RightMotionController"));
	_rightHand->SetupAttachment(_cameraComponent);
	
	SetReplicateMovement(true);
}

USkeletalMeshComponent* AAdvancedBoat::GetMeshComponent() const
{
	return _skeletalMeshComponent;
}

float AAdvancedBoat::GetMass() const
{
	if (_skeletalMeshComponent)
	{
		return _skeletalMeshComponent->GetMass();
	}

	return 0.f;
}

void AAdvancedBoat::NotifyActorBeginOverlap(AActor* otherActor)
{
	Super::NotifyActorBeginOverlap(otherActor);

	if (AWaterBody* waterBody = Cast<AWaterBody>(otherActor))
	{
		_currentWaterBodies.AddUnique(waterBody);
	}
}

void AAdvancedBoat::NotifyActorEndOverlap(AActor* otherActor)
{
	Super::NotifyActorEndOverlap(otherActor);

	if (AWaterBody* waterBody = Cast<AWaterBody>(otherActor))
	{
		_currentWaterBodies.Remove(waterBody);
	}
}

const TArray<AWaterBody*>& AAdvancedBoat::GetCurrentWaterBodies() const
{
	return _currentWaterBodies;
}

void AAdvancedBoat::SetupPlayerInputComponent(UInputComponent* playerInputComponent)
{
	Super::SetupPlayerInputComponent(playerInputComponent);

	playerInputComponent->BindAxis("Steer", this, &AAdvancedBoat::Steer);
	playerInputComponent->BindAxis("RotateMainSail", this, &AAdvancedBoat::RotateMainSail);
}

void AAdvancedBoat::BeginPlay()
{
	Super::BeginPlay();

	FTransform rootTransform = _skeletalMeshComponent->GetBoneTransform(_skeletalMeshComponent->GetBoneIndex("b_root"));
	FTransform sailTransform = _skeletalMeshComponent->GetBoneTransform(_skeletalMeshComponent->GetBoneIndex("b_sail"));

	_mainSailConstraint->SetConstrainedComponents(_skeletalMeshComponent, "b_sail", _skeletalMeshComponent, "b_root");

	_mainSailConstraint->SetAngularSwing1Limit(EAngularConstraintMotion::ACM_Limited, 80.f);
	_mainSailConstraint->SetAngularSwing2Limit(EAngularConstraintMotion::ACM_Locked, 0.f);
	_mainSailConstraint->SetAngularTwistLimit(EAngularConstraintMotion::ACM_Locked, 0.f);

	_mainSailConstraint->SetLinearXLimit(ELinearConstraintMotion::LCM_Locked, 0.f);
	_mainSailConstraint->SetLinearYLimit(ELinearConstraintMotion::LCM_Locked, 0.f);
	_mainSailConstraint->SetLinearZLimit(ELinearConstraintMotion::LCM_Locked, 0.f);

	_mainSailConstraint->SetAngularDriveMode(EAngularDriveMode::TwistAndSwing);
}

void AAdvancedBoat::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	
	if (_advancedBuoyancyComponent != nullptr && _skeletalMeshComponent != nullptr)
	{
		if (_advancedBuoyancyComponent->IsUnderWater())
		{
			_skeletalMeshComponent->AddForceAtLocation(_forceMagnitude * (_skeletalMeshComponent->GetForwardVector() * FVector(1.f, 1.f, 0.f)).GetSafeNormal(), _skeletalMeshComponent->GetComponentTransform().TransformPosition(_forcePosition));
		}
	}

	_wantedSailAngle = FMath::Clamp(_wantedSailAngle + 20.f * deltaTime * _currentWantedRotation, -80.f, 80.f);

	_mainSailConstraint->SetAngularOrientationTarget(FRotator(0.f, _wantedSailAngle, 0.f));

	float currentSailAngle = _skeletalMeshComponent->GetBoneQuaternion(TEXT("b_sail"), EBoneSpaces::ComponentSpace).Rotator().Yaw;
	if (FMath::Abs(currentSailAngle - _wantedSailAngle) <= 1.f)
	{
		_mainSailConstraint->SetAngularDriveParams(5000.f, 5000.f, 100000.f);
	}
	else
	{
		_mainSailConstraint->SetAngularDriveParams(5000.f, 100.f, 100000.f);
	}
}

FVector AAdvancedBoat::GetWaterRelativeVelocityAtPoint(FVector point) const
{
	if (_skeletalMeshComponent == nullptr)
	{
		return FVector::ZeroVector;
	}

	FVector waterVelocity = FVector::ZeroVector;
	for (AWaterBody* waterBody : _currentWaterBodies)
	{
		if (waterBody == nullptr)
		{
			continue;
		}

		EWaterBodyQueryFlags queryFlags = EWaterBodyQueryFlags::ComputeVelocity;
		FWaterBodyQueryResult queryResult = waterBody->QueryWaterInfoClosestToWorldLocation(point, queryFlags);
		if (queryResult.GetVelocity().Size() > waterVelocity.Size())
		{
			waterVelocity = queryResult.GetVelocity();
		}
	}

	return _skeletalMeshComponent->GetPhysicsLinearVelocityAtPoint(point) - waterVelocity;
}

FVector AAdvancedBoat::GetApparentWindVector() const
{
	if (_skeletalMeshComponent == nullptr)
	{
		return FVector::ZeroVector;
	}

	return -_skeletalMeshComponent->GetPhysicsLinearVelocity() + 2000.f * FVector(1.f, 0.f, 0.f);
}

void AAdvancedBoat::Steer(float value)
{
	float velSize = FMath::Abs(GetWaterRelativeVelocityAtPoint(_skeletalMeshComponent->GetComponentTransform().TransformPosition(FVector::ZeroVector)) | _skeletalMeshComponent->GetForwardVector());
	_skeletalMeshComponent->AddForceAtLocation(value * FMath::Clamp(velSize / 1000.f, 0.f, 1.f) * _steerForceMagnitude * (_skeletalMeshComponent->GetRightVector() * FVector(1.f, 1.f, 0.f)).GetSafeNormal(), _skeletalMeshComponent->GetComponentTransform().TransformPosition(_steerForcePosiiton));
}

void AAdvancedBoat::RotateMainSail(float value)
{
	_currentWantedRotation = value;
}
