#pragma once

struct FTriangleForce
{
	FVector Vector;
	FVector Point;
	FTriangleForce(const FVector& vector, const FVector& point)
		: Vector(vector), Point(point)
	{
	}
};

struct FBuoyantMeshVertex
{
	// Height above water, negative if below
	float Height = 0.f;

	FVector LocalPosition = FVector::ZeroVector;
	FVector Position = FVector::ZeroVector;

	FBuoyantMeshVertex(const FVector& localPosition);
	FBuoyantMeshVertex(const FVector& position, float height);

	bool IsUnderwater() const;
};

struct FBuoyantMeshTriangleData
{
	// Constants that do not change
	int32 A = 0;
	int32 B = 0;
	int32 C = 0;

	float Area = 0.f;

	// Data that depends on boat movement
	FVector Normal = FVector::ZeroVector;

	float PreviousSubmergedArea = 0.f;
	float SubmergedArea = 0.f;

	FBuoyantMeshVertex Center = {FVector::ZeroVector, 0.f};

	FVector PreviousVelocity = FVector::ZeroVector;
	FVector Velocity = FVector::ZeroVector;

	FBuoyantMeshTriangleData(int32 a, int32 b, int32 c, float area);
};

struct FBuoyantMeshSubmergedTriangle
{
	FVector A;
	FVector B;
	FVector C;

	FVector Normal;

	int32 OriginalTriangleIndex;

	FBuoyantMeshSubmergedTriangle(const FVector& a, const FVector& b, const FVector& c, const FVector& normal);

	FVector GetCenter() const;

	float GetArea() const;

	static FVector GetHydrostaticForce(float waterDensity, float gravityMagnitude, const FBuoyantMeshTriangleData& triangleData);

	static FVector GetHydrodynamicForce(UWorld* world, float waterDensity, float cf, float cPD1, float cPD2, float fP, float cSD1, float cSD2, float fS, float p, float maxSlammingAcceleration, float boatArea, float boatMass, float deltaTime, const FBuoyantMeshTriangleData& triangleData);
};

struct FBuoyantMeshTriangle
{
public:
	// Highest vertex
	FBuoyantMeshVertex H;
	// Middle vertex
	FBuoyantMeshVertex M;
	// Lowest vertex
	FBuoyantMeshVertex L;

	FVector Normal;

	FBuoyantMeshTriangle(const FBuoyantMeshVertex& h, const FBuoyantMeshVertex& m, const FBuoyantMeshVertex& l, const FVector& normal);

	FVector GetCenter() const;

	float GetArea() const;

	static FBuoyantMeshTriangle FromClockwiseVertices(const FBuoyantMeshVertex& a, const FBuoyantMeshVertex& b, const FBuoyantMeshVertex& c);
	void GetSubmergedTriangles(TArray<FBuoyantMeshSubmergedTriangle>& outSubmergedTriangles) const;

private:
	FVector FindSubmergedPart(const FBuoyantMeshVertex& start, const FBuoyantMeshVertex& end) const;
};

float GetTriangleArea(const FVector& a, const FVector& b, const FVector& c);

FVector GetTriangleCenter(const FVector& a, const FVector& b, const FVector& c);
