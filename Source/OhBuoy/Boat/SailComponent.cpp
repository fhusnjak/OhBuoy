#include "SailComponent.h"
#include "AdvancedBoat.h"
#include "DrawDebugHelpers.h"
#include "Math/NumericLimits.h"
#include "Net/UnrealNetwork.h"
#include "PhysXPublic.h"
#include "PhysicsEngine/BodySetup.h"
#include "AdvancedBuoyancyComponentShared.h"
#include "Components/LineBatchComponent.h"

float FSail::GetLiftCoefficientAtAngle(float angle) const
{
	float sign = 1.f;
	if (angle > 100.f)
	{
		angle = 180.f - angle;
		sign = -1.f;
	}
	return sign * (- 0.000000236451049f * FMath::Pow(angle, 4.f)
		   + 0.00006606934732f * FMath::Pow(angle, 3.f)
		   - 0.006590661422f * FMath::Pow(angle, 2.f)
		   + 0.2533863636f * FMath::Pow(angle, 1.f)
		   - 1.7391666f);
}

float FSail::GetDragCoefficientAtAngle(float angle) const
{
	if (angle > 90.f)
	{
		angle = 180.f - angle;
	}
	return + 0.000001168609f * FMath::Pow(angle, 3.f)
		   + 0.000055543124f * FMath::Pow(angle, 2.f)
		   + 0.0017943279f * FMath::Pow(angle, 1.f)
		   + 0.127066667f;
}

USailComponent::USailComponent(const FObjectInitializer& initializer)
	: Super(initializer)
{
	PrimaryComponentTick.TickGroup = TG_PrePhysics;
	PrimaryComponentTick.bCanEverTick = true;
	bAutoActivate = true;
	UActorComponent::SetComponentTickEnabled(true);
}

void USailComponent::BeginPlay()
{
	Super::BeginPlay();

	for (FSail& sail : _sails)
	{
		sail.Area = GetTriangleArea(sail.Point1, sail.Point2, sail.Point3);
	}
}

void USailComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

const TArray<FSail>& USailComponent::GetSails() const
{
	return _sails;
}

void USailComponent::TickComponent(float deltaTime, enum ELevelTick tickType, FActorComponentTickFunction *thisTickFunction)
{
	Super::TickComponent(deltaTime, tickType, thisTickFunction);

	AAdvancedBoat* boat = Cast<AAdvancedBoat>(GetOwner());
	if (boat == nullptr || boat->GetMeshComponent() == nullptr)
	{
		return;
	}

	FVector apparentWindVector = boat->GetApparentWindVector();

	_sails[1].Yaw = boat->GetMeshComponent()->GetBoneQuaternion(TEXT("b_sail"), EBoneSpaces::ComponentSpace).Rotator().Yaw;

	FSail sail = _sails[1];
	float maxForce = FLT_MIN;
	float bestYaw = 0.f;
	for (float yaw = -80.f; yaw <= 80.f; yaw += 3.f)
	{
		sail.Yaw = yaw;
		float force = CalculateSailForce(sail, apparentWindVector) | boat->GetMeshComponent()->GetForwardVector();
		if (force > maxForce)
		{
			maxForce = force;
			bestYaw = yaw;
		}
	}

	FVector initialPoint = GetComponentTransform().TransformPosition(FVector(39.85f, 0.f, 70.287598f));
	FVector sailVector = GetComponentTransform().TransformVectorNoScale(FRotator(0.f, bestYaw, 0.f).RotateVector(FVector::ForwardVector)).GetSafeNormal();
	GetWorld()->LineBatcher->DrawLine(initialPoint, initialPoint - 250 * sailVector, FLinearColor::Red, SDPG_World, 5.f, -1.f);
		
	for (const FSail& s : _sails)
	{
		FVector resultForce = CalculateSailForce(s, apparentWindVector);
		FVector forceLocation = GetComponentTransform().TransformPosition((s.Point1 + s.Point2 + s.Point3) / 3.f - FVector(0.f, 0.f, 200.f));
		boat->GetMeshComponent()->AddForceAtLocation(resultForce, forceLocation);

		//UE_LOG(LogTemp, Warning, TEXT("Angle = %f, Lift = %f, Drag = %f"), angle, liftCoeficient, dragCoeficient);
		//UE_LOG(LogTemp, Warning, TEXT("Lift force = %f, Drag force = %f, Result force = %f"), sail.GetLiftCoefficientAtAngle(30.f), sail.GetDragCoefficientAtAngle(90.f) , resultForce.Size());

		//DrawDebugDirectionalArrow(GetWorld(), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 120.f)), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 100.f)) + 200 * sailVector, 40.f, FColor::Orange, false, -1.f, 0, 5.f);
		//DrawDebugDirectionalArrow(GetWorld(), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 120.f)), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 100.f)) + liftForce / 1000000.f, 40.f, FColor::Red, false, -1.f, 0, 5.f);
	}

	//DrawDebugDirectionalArrow(GetWorld(), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 100.f)) - 200 * apparentWindVector.GetSafeNormal(), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 120.f)), 40.f, FColor::Green, false, -1.f, 0, 5.f);

	//DrawDebugDirectionalArrow(GetWorld(), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 120.f)), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 100.f)) + 200 * CalculateLiftDirection(FVector(1.f, 0.f, 0.f), GetComponentTransform().GetUnitAxis(EAxis::X)), 40.f, FColor::Red, false, -1.f, 0, 5.f);
	//DrawDebugDirectionalArrow(GetWorld(), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 120.f)), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 100.f)) + 200 * FVector(1.f, 0.f, 0.f), 40.f, FColor::Green, false, -1.f, 0, 5.f);
	//DrawDebugDirectionalArrow(GetWorld(), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 120.f)), GetComponentTransform().TransformPosition(FVector(0.f, 0.f, 100.f)) + 200 * GetComponentTransform().GetUnitAxis(EAxis::X), 40.f, FColor::Blue, false, -1.f, 0, 5.f);
}

FVector USailComponent::CalculateSailForce(const FSail& sail, FVector apparentWindVector)
{
	float windVelocity = apparentWindVector.Size();
	FVector sailVector = GetComponentTransform().TransformVectorNoScale(FRotator(0.f, sail.Yaw, 0.f).RotateVector(FVector::ForwardVector)).GetSafeNormal();
	
	float angle = FMath::RadiansToDegrees(FMath::Acos(sailVector.GetSafeNormal() | -apparentWindVector.GetSafeNormal())) * FMath::Sign((sailVector.GetSafeNormal() ^ -apparentWindVector.GetSafeNormal()).Z);
	float liftCoeficient = sail.GetLiftCoefficientAtAngle(FMath::Abs(angle));
	float dragCoeficient = sail.GetDragCoefficientAtAngle(FMath::Abs(angle));
	
	FVector liftForceDirection = FRotator(0.f, FMath::Sign(angle) * 90.f, 0.f).RotateVector(apparentWindVector);
	FVector liftForce = liftForceDirection.GetSafeNormal() * CalculateSailForce(liftCoeficient, windVelocity, sail.Area);
	FVector dragForce = apparentWindVector.GetSafeNormal() * CalculateSailForce(dragCoeficient, windVelocity, sail.Area);
	
	return dragForce + liftForce;
}

float USailComponent::CalculateSailForce(float coefficient, float windVelocity, float sailArea)
{
	// Air density in kg / cm^-3
	static const float airRho = 0.0000012f;
	return 0.5f * airRho * windVelocity * windVelocity * sailArea * coefficient;
}
