#pragma once

#include "SailComponent.generated.h"

USTRUCT()
struct FSail
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	FVector Point1;

	UPROPERTY(EditAnywhere)
	FVector Point2;

	UPROPERTY(EditAnywhere)
	FVector Point3;

	float Yaw = 0.f;

	float Area = 1.f;

	float GetLiftCoefficientAtAngle(float angle) const;
	float GetDragCoefficientAtAngle(float angle) const;
};

UCLASS(Blueprintable, ClassGroup = (Custom), config = Engine, meta = (BlueprintSpawnableComponent))
class OHBUOY_API USailComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	USailComponent(const FObjectInitializer& initializer);

	virtual void BeginPlay() override;

	virtual void TickComponent(float deltaTime, enum ELevelTick tickType, FActorComponentTickFunction *thisTickFunction) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	const TArray<FSail>& GetSails() const;

	FVector GetApparentWindVector() const;

private:
	UPROPERTY(EditAnywhere)
	TArray<FSail> _sails;

private:
	FVector CalculateSailForce(const FSail& sail, FVector apparentWindVector);
	float CalculateSailForce(float coefficient, float windVelocity, float sailArea);
	
};
