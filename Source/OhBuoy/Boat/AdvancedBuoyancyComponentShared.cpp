#include "AdvancedBuoyancyComponentShared.h"

FBuoyantMeshVertex::FBuoyantMeshVertex(const FVector& localPosition)
	: LocalPosition(localPosition)
{
}

FBuoyantMeshVertex::FBuoyantMeshVertex(const FVector& position, float height)
	: Height(height), Position(position)
{
}

bool FBuoyantMeshVertex::IsUnderwater() const
{
	return Height < 0.f;
}

FBuoyantMeshTriangleData::FBuoyantMeshTriangleData(int32 a, int32 b, int32 c, float area)
	: A(a), B(b), C(c), Area(area)
{
}

FBuoyantMeshSubmergedTriangle::FBuoyantMeshSubmergedTriangle(const FVector& a, const FVector& b, const FVector& c, const FVector& normal)
	: A(a), B(b), C(c), Normal(normal)
{
}

FVector FBuoyantMeshSubmergedTriangle::GetCenter() const
{
	return GetTriangleCenter(A, B, C);
}

float FBuoyantMeshSubmergedTriangle::GetArea() const
{
	return GetTriangleArea(A, B, C);
}

FVector FBuoyantMeshSubmergedTriangle::GetHydrostaticForce(float waterDensity, float gravityMagnitude, const FBuoyantMeshTriangleData& triangleData)
{
	// 0.000001f is used to convert cm^3 -> m^3
	// Force is in Newtons
	return waterDensity * gravityMagnitude * triangleData.Center.Height * triangleData.SubmergedArea * 0.000001f * (triangleData.Normal | FVector::UpVector) * FVector::UpVector;
}

FVector FBuoyantMeshSubmergedTriangle::GetHydrodynamicForce(UWorld* world, float waterDensity, float cf, float cPD1, float cPD2, float fP, float cSD1, float cSD2, float fS, float p, float maxSlammingAcceleration, float boatArea, float boatMass, float deltaTime, const FBuoyantMeshTriangleData& triangleData)
{
	float centerVelocitySize = triangleData.Velocity.Size();
	FVector centerVelocityNormalized = triangleData.Velocity.GetSafeNormal();

	FVector hydrodynamicForce = FVector::ZeroVector;

	// Viscous resistance
	FVector tangentVelocity = triangleData.Normal ^ (triangleData.Velocity ^ triangleData.Normal);
	FVector viscousResistance = -0.000000005f * waterDensity * cf * triangleData.SubmergedArea * centerVelocitySize * centerVelocitySize * tangentVelocity.GetSafeNormal();
	hydrodynamicForce += viscousResistance;

	// Pressure drag force
	float cosTheta = triangleData.Normal | centerVelocityNormalized;
	if (cosTheta > 0.f)
	{
		hydrodynamicForce += -(cPD1 * centerVelocitySize * 0.01f + cPD2 * centerVelocitySize * centerVelocitySize * 0.0001f) * triangleData.SubmergedArea * 0.0001f * FMath::Pow(cosTheta, fP) * triangleData.Normal;
	}
	else
	{
		hydrodynamicForce += (cSD1 * centerVelocitySize * 0.01f + cSD2 * centerVelocitySize * centerVelocitySize * 0.0001f) * triangleData.SubmergedArea * 0.0001f * FMath::Pow(-cosTheta, fS) * triangleData.Normal;
	}

	// Slamming force
	if (cosTheta > 0.f && triangleData.Area > 0.f)
	{
		FVector previousDV = triangleData.PreviousSubmergedArea * triangleData.PreviousVelocity;
		FVector dV = triangleData.SubmergedArea * triangleData.Velocity;

		FVector acceleration = (dV - previousDV) / (triangleData.Area * deltaTime);
		float accelerationSize = acceleration.Size() * 0.01f;

		FVector stopForce = boatMass * triangleData.Velocity * (2.f * triangleData.SubmergedArea / boatArea) * 0.01f;
		
		FVector slammingForce = -FMath::Pow(FMath::Clamp(accelerationSize / maxSlammingAcceleration, 0.f, 1.f), p) * cosTheta * stopForce;
		
		hydrodynamicForce += slammingForce;

		/*
		if (accelerationSize * cosTheta > maxSlammingAcceleration)
		{
			DrawDebugPoint(world, triangleData.Center.Position, 30.f, FColor::Red, false, -1.f, 0);
		}*/
	}

	return hydrodynamicForce;
}

FBuoyantMeshTriangle::FBuoyantMeshTriangle(const FBuoyantMeshVertex& h, const FBuoyantMeshVertex& m, const FBuoyantMeshVertex& l, const FVector& normal) 
	: H(h), M(m), L(l), Normal(normal)
{
}

FVector FBuoyantMeshTriangle::GetCenter() const
{
	return GetTriangleCenter(H.Position, M.Position, L.Position);
}

float FBuoyantMeshTriangle::GetArea() const
{
	return GetTriangleArea(H.Position, M.Position, L.Position);
}

FBuoyantMeshTriangle FBuoyantMeshTriangle::FromClockwiseVertices(const FBuoyantMeshVertex& a, const FBuoyantMeshVertex& b, const FBuoyantMeshVertex& c)
{
	const FVector normal = FVector::CrossProduct(b.Position - a.Position, c.Position - a.Position).GetSafeNormal();

	const FBuoyantMeshVertex* h = &a;
	const FBuoyantMeshVertex* m = &b;
	const FBuoyantMeshVertex* l = &c;

	if (h->Height < l->Height)
	{
		Swap(h, l);
	}
	if (h->Height < m->Height)
	{
		Swap(h, m);
	}
	if (l->Height > m->Height)
	{
		Swap(l, m);
	}

	return {*h, *m, *l, normal};
}

void FBuoyantMeshTriangle::GetSubmergedTriangles(TArray<FBuoyantMeshSubmergedTriangle>& outSubmergedTriangles) const
{
	if (H.IsUnderwater() && M.IsUnderwater() && L.IsUnderwater())
	{
		outSubmergedTriangles.Emplace(H.Position, M.Position, L.Position, Normal);
	}
	else if (M.IsUnderwater() && L.IsUnderwater())
	{
		const FVector subM = FindSubmergedPart(M, H);
		outSubmergedTriangles.Emplace(M.Position, subM, L.Position, Normal);
		const FVector subL = FindSubmergedPart(L, H);
		outSubmergedTriangles.Emplace(subM, subL, L.Position, Normal);
	}
	else if (L.IsUnderwater())
	{
		outSubmergedTriangles.Emplace(FindSubmergedPart(L, H), FindSubmergedPart(L, M), L.Position, Normal);
	}
}

FVector FBuoyantMeshTriangle::FindSubmergedPart(const FBuoyantMeshVertex& start, const FBuoyantMeshVertex& end) const
{
	const float t = -start.Height / (end.Height - start.Height);
	return start.Position + t * (end.Position - start.Position);
}

float GetTriangleArea(const FVector& a, const FVector& b, const FVector& c)
{
	const auto ab = FVector::Dist(a, b);
	const auto bc = FVector::Dist(b, c);
	const auto ca = FVector::Dist(c, a);
	const auto s = (ab + bc + ca) / 2.f;
	return FMath::Sqrt(s * (s - ab) * (s - bc) * (s - ca));
}

FVector GetTriangleCenter(const FVector& a, const FVector& b, const FVector& c)
{
	return (a + b + c) / 3.f;
}
