#pragma once

#include "WaterBodyActor.h"
#include "AdvancedBoat.generated.h"

class UAdvancedBuoyancyComponent;
class USailComponent;
class UPhysicsConstraintComponent;
class USpringArmComponent;
class UCameraComponent;
class UMotionControllerComponent;

UCLASS(Blueprintable)
class OHBUOY_API AAdvancedBoat : public APawn
{
	GENERATED_BODY()

public:
	AAdvancedBoat(const FObjectInitializer& initializer);

	USkeletalMeshComponent* GetMeshComponent() const;

	float GetMass() const;

	virtual void NotifyActorBeginOverlap(AActor* otherActor) override;
	virtual void NotifyActorEndOverlap(AActor* otherActor) override;

	const TArray<AWaterBody*>& GetCurrentWaterBodies() const;

	virtual void SetupPlayerInputComponent(UInputComponent* playerInputComponent) override;

	virtual void BeginPlay() override;

	virtual void Tick(float deltaTime) override;

	FVector GetWaterRelativeVelocityAtPoint(FVector point) const;

	FVector GetApparentWindVector() const;

private:
	UPROPERTY(VisibleAnywhere, Category = "Physics | Buoyancy")
	USkeletalMeshComponent* _skeletalMeshComponent;

	UPROPERTY(VisibleAnywhere, Category = "Physics | Buoyancy")
	UAdvancedBuoyancyComponent* _advancedBuoyancyComponent;

	UPROPERTY(VisibleAnywhere, Category = "Physics | Lift")
	USailComponent* _sailComponent;

	UPROPERTY(VisibleAnywhere, Category = "Physics")
	UPhysicsConstraintComponent* _mainSailConstraint;

	UPROPERTY(EditAnywhere)
	FVector _forcePosition = FVector(-240.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere)
	FVector _steerForcePosiiton = FVector(-240.f, 0.f, -10.f);

	UPROPERTY(EditAnywhere)
	float _forceMagnitude = 1000000.f;

	UPROPERTY(EditAnywhere)
	float _steerForceMagnitude = 100000.f;

	UPROPERTY(EditAnywhere)
	USpringArmComponent* _springArmComponent;

	UPROPERTY(EditAnywhere)
	UCameraComponent* _cameraComponent;

	UPROPERTY(EditAnywhere)
	UMotionControllerComponent* _leftHand;

	UPROPERTY(EditAnywhere)
	UMotionControllerComponent* _rightHand;

	UPROPERTY(Transient)
	TArray<AWaterBody*> _currentWaterBodies;

	float _wantedSailAngle = 0.f;
	float _currentWantedRotation = 0.f;

private:
	void Steer(float value);
	void RotateMainSail(float value);

};
