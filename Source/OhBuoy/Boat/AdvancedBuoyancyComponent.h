#pragma once

#include "AdvancedBuoyancyComponentShared.h"
#include "AdvancedBuoyancyComponent.generated.h"

UCLASS(Blueprintable, ClassGroup = (Custom), config = Engine, meta = (BlueprintSpawnableComponent))
class OHBUOY_API UAdvancedBuoyancyComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	UAdvancedBuoyancyComponent(const FObjectInitializer& initializer);

public:
	virtual void TickComponent(float deltaTime, enum ELevelTick tickType, FActorComponentTickFunction *thisTickFunction) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	bool IsUnderWater() const;

private:
	bool _initialized;

	float _gravityMagnitude = 9.81f;
	
	// sea density = 1025 kg m^-3
	float _waterDensity = 1025.f;

	UPROPERTY(EditAnywhere, Category = "Physics")
	// 1 * 10^-6 m^2 s^-1, at 20 degrees
	float _waterKinematicViscosity = 0.000001f;

	UPROPERTY(EditAnywhere, Category = "Physics")
	float _cPD1 = 10.f;
	
	UPROPERTY(EditAnywhere, Category = "Physics")
	float _cPD2 = 10.f;
	
	UPROPERTY(EditAnywhere, Category = "Physics")
	float _fP = 0.5f;
	
	UPROPERTY(EditAnywhere, Category = "Physics")
	float _cSD1 = 10.f;
	
	UPROPERTY(EditAnywhere, Category = "Physics")
	float _cSD2 = 10.f;;
	
	UPROPERTY(EditAnywhere, Category = "Physics")
	float _fS = 0.5f;

	UPROPERTY(EditAnywhere, Category = "Physics")
	float _maxAcceleration = 1.f;

	UPROPERTY(EditAnywhere, Category = "Physics")
	float _p = 2.f;

	TArray<FBuoyantMeshVertex> _vertices;
	TArray<FBuoyantMeshTriangleData> _triangleData;

	float _buoyantMeshArea = 0.f;

	bool _isUnderWater;

private:
	void DrawDebugTriangle(const FVector& a, const FVector& b, const FVector& c, const FColor& color, const float thickness);

	void Initialize();

	void UpdateTriangleData(const FBuoyantMeshSubmergedTriangle& submergedTriangle);

	FTriangleForce GetSubmergedTriangleForce(const FBuoyantMeshTriangleData& triangleData, float cf, float deltaTime) const;
	void ApplyMeshForces(float deltaTime);
	void ApplyMeshForce(const FTriangleForce& force);

	float GetHeightAboveWater(const FVector& position) const;
};
