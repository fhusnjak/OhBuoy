// Copyright Epic Games, Inc. All Rights Reserved.

#include "OhBuoy.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, OhBuoy, "OhBuoy" );
