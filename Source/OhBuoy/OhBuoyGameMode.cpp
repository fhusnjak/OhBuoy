#include "OhBuoyGameMode.h"
#include "Boat/AdvancedBoat.h"

UClass* AOhBuoyGameMode::GetDefaultPawnClassForController_Implementation(AController* inController)
{
	if (inController->IsLocalController())
	{
		return ServerPawnClass.Get();
	}

	return ClientPawnClass.Get();
}
