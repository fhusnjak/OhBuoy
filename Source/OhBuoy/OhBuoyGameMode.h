#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "OhBuoyGameMode.generated.h"

UCLASS()
class OHBUOY_API AOhBuoyGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	UClass* GetDefaultPawnClassForController_Implementation(AController* inController) override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<APawn> ServerPawnClass;

	UPROPERTY(EditAnywhere)
	TSubclassOf<APawn> ClientPawnClass;
};
