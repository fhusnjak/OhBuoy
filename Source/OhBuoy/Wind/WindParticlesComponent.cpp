// Fill out your copyright notice in the Description page of Project Settings.

#include "WindParticlesComponent.h"
#include <OhBuoy/Boat/AdvancedBoat.h>

#define DEBUGMESSAGE(x, ...) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT(x), __VA_ARGS__));}

#define INITIAL_VELOCITY_PARAM_NAME "InitialVelocityDistribution"

// Sets default values for this component's properties
UWindParticlesComponent::UWindParticlesComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UWindParticlesComponent::BeginPlay()
{
	Super::BeginPlay();
	SetParticleSpeed(_particleSpeed);
}

void UWindParticlesComponent::SetParticleSpeed(float speed) {
	float velSpread = 0.3;

	this->SetVectorRandParameter(
		INITIAL_VELOCITY_PARAM_NAME,
		{ speed * (1 + velSpread), 0, 200 },
		{ speed * (1 - velSpread), 0, 100 }
	);

	_particleSpeed = speed;
}

// Called every frame
void UWindParticlesComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	AAdvancedBoat* owner = GetOwner<AAdvancedBoat>();
	if ( owner == nullptr ) 
		return;

	FVector ownerLocation = owner->GetActorLocation();
	FVector ownerVelocity = owner->GetWaterRelativeVelocityAtPoint( ownerLocation );
	FRotator ownerRotation = owner->GetActorRotation();
	
	FVector windDirection;
	float windSpeed, minGustAmt, maxGustAmt;

	auto world = GetWorld();
	if ( world == nullptr ) 
		return;

	world->Scene->GetWindParameters( ownerLocation, windDirection, windSpeed, minGustAmt, maxGustAmt );

	float particleSpawnToDestinationTime = _particleSpawnDistance / _particleSpeed;

	FVector newLocation = -_particleSpawnDistance * FVector(windDirection.X, windDirection.Y, 0)
		+ ownerRotation.RotateVector(_particleTargetDisplacement)
		+ ownerLocation
		+ ownerVelocity * particleSpawnToDestinationTime;

	this->SetWorldLocation( newLocation );
	this->SetWorldRotation( windDirection.Rotation() );
}

