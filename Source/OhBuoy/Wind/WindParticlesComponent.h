// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include <Runtime/Engine/Classes/Particles/ParticleSystemComponent.h>
#include "WindParticlesComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OHBUOY_API UWindParticlesComponent : public UParticleSystemComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWindParticlesComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void SetParticleSpeed(float speed);
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
private:
	UPROPERTY(EditAnywhere, Category = "Particle effect")
	float _particleSpeed;

	UPROPERTY(EditAnywhere, Category = "Particle effect")
	float _particleSpawnDistance;

	UPROPERTY(EditAnywhere, Category = "Particle effect")
	FVector _particleTargetDisplacement;
};
